#ifndef _LOGGER_H
#define _LOGGER_H
#include <stdio.h>
#include <stdlib.h>
struct logger {
  FILE* _logFile;
  void (*doInfo)(struct logger* this, const char* message);
  void (*free)(struct logger* this);
} typedef logger_t;

logger_t* logger_new(const char* logFile);
#endif
