#include <stdio.h>
#include <string.h>
#include "terminal.h"
#include "command.h"
#include "point.h"
#include "font.h"

void drawChar(char* c, struct winsize* screenSize, struct point* position){
	storeCursorPosition();
	struct point p;
	p.x = position->x;
	p.y = position->y;
	setCursorPosition(&p);
	char* space = " ";
	for(int y = 0; y < FONT_HEIGHT; y++){
		for(int x = 0; x < FONT_WIDTH; x++){
			int yy = y + ((c[0] - 'a') * FONT_HEIGHT);
			bool write = font[yy][x];
			//char* _c = write ? c : space;
			char* _c = write ? "*" : " ";
			writeChar(_c);
			p.x++;
			setCursorPosition(&p);
		}
		p.x = position->x;
		p.y++;
		setCursorPosition(&p);
	}
	restoreCursorPosition();
}

void doLose(struct winsize* screenSize){
	char* s = "lose";
	struct point p = {10, 10};
	for(int i = 0; i < strlen(s); i++){
		char c[2] = {s[i], 0};
		drawChar(c, screenSize, &p);
		p.x += FONT_WIDTH;
	}
}

int main(int argc, char* argv[]){
	enableRawMode();
	struct winsize* screenSize = getScreenSize();
	doLose(screenSize);
}
