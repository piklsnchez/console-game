#ifndef _TERMINAL_H
#define _TERMINAL_H
void terminal_init();
void enableRawMode();
void enableCookedMode();
struct winsize* getScreenSize();
void waitForInput();
int getBuffered();
char* readChar();
char* readCharNoWait();
void writeChars(char* c);
void fillScreen(struct winsize screenSize, const char c);
#endif
