#include <unistd.h>
#include <stdbool.h>
#include "point.h"

bool pointEquals(struct point* p1, struct point* p2){
	return NULL != p1 && NULL != p2 && p1->x == p2->x && p1->y == p2->y;
}

int pointNumberOf(struct point* array[], int size){
	int total = 0;
	for(int i = 0; i < size; i++){
		if(NULL != array[i]){
			total++;
		}
	}
	return total;
}
