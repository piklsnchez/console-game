#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/kd.h>
#include <linux/keyboard.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include "readInput.h"

void init(){
  int fd = open("/dev/input/event0", O_RDONLY);
  if(-1 == fd){
    printf("ERROR: unable to open input device\n");
    return;
  }
  struct input_event event;
  while(0 < read(fd, &event, sizeof(struct input_event))){
    if(1 == event.type){
      printf("type: %d, code: %d, value: %d\n", event.type, event.code, event.value);
    }
  }
}
