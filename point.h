#ifndef _POINT_H
#define _POINT_H
struct point{
	int x;
	int y;
};
bool pointEquals(struct point* p1, struct point* p2);
int pointNumberOf(struct point* array[], int size);
#endif
