#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "at.h"

#include "private.h"

#include <execinfo.h>//stacktrace
#include <signal.h>//stacktrace

char* imapConnect(int fd, const char* domain, int port);
char* imapLogin(int fd);
char* imapGetNamespaces(int fd);
char* imapList(int fd, const char* folder);
char* imapSearch(int fd, const char* search);
char* imapSelect(int fd, const char* folder);
char* imapFetch(int fd, const char* fetch);
char* imapLogout(int fd);
void dump(int sig);

static const char LOGIN[]  = "a%d login %s %s\r\n";
static const char LOGOUT[] = "a%d logout\r\n";

static const char DOMAIN_NAME[] = "imap.gmail.com";
static const char IP_ADDRESS[]  = "142.250.101.109";//gmail
static const int  PORT          = 993;

int message = 0;

int main(int argc, char* argv[]){
	signal(SIGSEGV, dump);

	int fd = initTty();//error handling within

#ifndef __DEBUG
	if(!apEchoOff(fd)){
		fprintf(stderr, "ERROR: couldn't turn echo off\n");
		return EXIT_FAILURE;
	}
#else
	if(!apEchoOn(fd)){
		fprintf(stderr, "ERROR: couldn't turn echo on\n");
		return EXIT_FAILURE;
	}	
#endif

	int apStatus = getApStatus(fd);
	if(1 != apStatus){
		bool success = setApStationMode(fd);
		if(!success){
			OUTPUT;
			fprintf(stderr, "ERROR: unable to set station mode");
			exit(EXIT_FAILURE);
		}
		success = apConnect(fd);
		if(!success){
			OUTPUT;
			fprintf(stderr, "ERROR: unable to connect");
			exit(EXIT_FAILURE);
		}
	}
	
	int sslBufferSize = getSslBufferSize(fd);
	if(4096 != sslBufferSize){
		char* response = setSslBufferSize(fd, 4096);
		free(response);
	}
	char* response;
	response = imapConnect(fd, DOMAIN_NAME, PORT);
	if(NULL != response){
		write(STDOUT_FILENO, response, strlen(response));
		write(STDOUT_FILENO, "\n", 1);
		free(response);
	}
	response = imapLogin(fd);
	if(NULL != response){
		write(STDOUT_FILENO, response, strlen(response));
		free(response);
	}
	response = imapGetNamespaces(fd);
	if(NULL != response){
		write(STDOUT_FILENO, response, strlen(response));
		free(response);
	}
	response = imapSelect(fd, "inbox");
	if(NULL != response){
		write(STDOUT_FILENO, response, strlen(response));
		free(response);
	}
	response = imapSearch(fd, "UNSEEN");
	if(NULL != response){
		write(STDOUT_FILENO, response, strlen(response));
		free(response);
	}
	response = imapFetch(fd, "256 rfc822");
	if(NULL != response){
		write(STDOUT_FILENO, response, strlen(response));
		free(response);
	}
	response = imapLogout(fd);
	if(NULL != response){
		write(STDOUT_FILENO, response, strlen(response));
		free(response);
	}
	return 0;
}

char* imapConnect(int fd, const char* domain, int port){
	PRINT_FUNC;
	char* response = NULL;
	int tcpConnectionStatus = getConnectionStatus(fd);
	if(3 != tcpConnectionStatus){
		char* ipAddress = dnsLookup(fd, domain);
		if(NULL == ipAddress){
			OUTPUT;
			fprintf(stderr, "couldn't get ip address");
			fflush(stderr);
			return NULL;
		}
		response = tcpConnect(fd, ipAddress, PORT, true);
		free(ipAddress);
		if(NULL != response){
			char* parsedResponse = parseResponse(response);
			free(response);
			response = parsedResponse;
		}
	}
	return response;
}

char* imapLogin(int fd){
	PRINT_FUNC;
	char* command  = newCommand(LOGIN, message++, SMTP_USER_NAME, SMTP_PASSWORD);
	char* response = tcpSendData(fd, command);
	free(command);
	return response;
}

char* imapGetNamespaces(int fd){
	PRINT_FUNC;
	char* command  = newCommand("a%d namespace\r\n", message++);
	char* response = tcpSendData(fd, command);
	free(command);
	return response;
}

char* imapList(int fd, const char* folder){
	PRINT_FUNC;
	char* command  = newCommand("a%d list \"%s\" \"*\"\r\n", message++, folder);
	char* response = tcpSendData(fd, command);
	free(command);
	return response;
}

char* imapSearch(int fd, const char* search){
	PRINT_FUNC;
	char* command  = newCommand("a%d search %s\r\n", message++, search);
	char* response = tcpSendData(fd, command);
	free(command);
	return response;
}

char* imapSelect(int fd, const char* folder){
	PRINT_FUNC;
	char* command  = newCommand("a%d select %s\r\n", message++, folder);
	char* response = tcpSendData(fd, command);
	free(command);
	return response;
}

char* imapFetch(int fd, const char* fetch){
	PRINT_FUNC;
	char* command  = newCommand("a%d fetch %s\r\n", message++, fetch);
	char* response = tcpSendData(fd, command);
	free(command);
	return response;
}

char* imapLogout(int fd){
	PRINT_FUNC;
	char* command  = newCommand(LOGOUT, message++);
	char* response = tcpSendData(fd, command);
	free(command);
	return response;
}

void dump(int sig){
  void* array[10];
  size_t size = backtrace(array, 10);
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
//try to disconnect
	imapLogout(3);
  exit(EXIT_FAILURE);	
}
