#ifndef AT_H
#define AT_H
#define OUTPUT printf("%s(%d) %s: ", __FILE__, __LINE__,  __func__);\
	fflush(stdout)

#ifdef __DEBUG
#define PRINT_FUNC \
	write(STDOUT_FILENO, __func__, strlen(__func__));\
	write(STDOUT_FILENO, "\n", 1)
#else
#define PRINT_FUNC
#endif
//open tty set mode and return fd
int   initTty();
char* apRestart(int fd);
bool  apEchoOff(int fd);
bool  apEchoOn(int fd);
int   getApStatus(int fd);//0 = not connected, 1 = connected
int   getConnectionStatus(int fd);//2 = connected to an AP, 3 = TCP connected, 4 = TCP disconnected, 5 = AP not connected 
bool  setApStationMode(int fd);
bool  apConnect(int fd);
char* getIpAddress(int fd);
char* dnsLookup(int fd, const char* domain);
char* getSslConfiguration(int fd);
char* setSslBufferSize(int fd, int size);
int   getSslBufferSize(int fd);
char* configureSsl(int fd);
char* tcpConnect(int fd, const char* ipAddress, int port, bool ssl);
char* tcpDisconnect(int fd);
char* tcpSendData(int fd, const char* data);
char* apSendCommand(int fd, const char* command);
char* apSendCommand2(int fd, const char* command, const char* success);
char* newCommand(const char* format, ...);
bool  anyMatch(char* haystack, char* needles[]);
char* parseResponse(char* response);//TODO: this doesn't belong here
bool  endsWith(const char* string, const char* end);
#endif
