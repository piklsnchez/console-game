#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include "at.h"

#include <execinfo.h>//stacktrace
#include <signal.h>//stacktrace
//#define __DEBUG
void dump(int sig);

static const char DOMAIN_NAME[] = "www.google.com";
static const int  PORT          = 443;
static const char DATA[]        = "GET / HTTP/1.1\r\n\r\n\0";

int main(int argc, char* argv[]){
	signal(SIGSEGV, dump);

	int fd = initTty();//errors handled internal
	char* response = NULL;
#ifndef __DEBUG
	if(!apEchoOff(fd)){
		fprintf(stderr, "ERROR: couldn't turn echo off\n");
		return EXIT_FAILURE;
	}
#else
	if(!apEchoOn(fd)){
		fprintf(stderr, "ERROR: couldn't turn echo on\n");
		return EXIT_FAILURE;
	}	
#endif
	int apStatus = getApStatus(fd);//TODO: this can be combined via getConnectionStatus
	if(1 != apStatus){
		if(!setApStationMode(fd)){
			fprintf(stderr, "ERROR: unable to set station mode\n");
			return EXIT_FAILURE;
		}
		if(!apConnect(fd)){
			fprintf(stderr, "ERROR: unable to connect\n");
			return EXIT_FAILURE;
		}
	}
#ifdef __DEBUG
	response = getIpAddress(fd);
	if(NULL != response){
		write(STDOUT_FILENO, response, strlen(response));
		free(response);
	}
#endif
	
	int sslBufferSize = getSslBufferSize(fd);
	if(4096 != sslBufferSize){
		char* response = setSslBufferSize(fd, 4096);
		free(response);
	}

  int tcpConnectionStatus = getConnectionStatus(fd);
	if(3 == tcpConnectionStatus || 0 > tcpConnectionStatus){//3 = tcp connection already created, -1 = error
		response = tcpDisconnect(fd);
		if(NULL != response){
			write(STDOUT_FILENO, response, strlen(response));
			free(response);
		}
		if(0 > tcpConnectionStatus){
			return EXIT_FAILURE;
		}
	}

	char* ipAddress = dnsLookup(fd, DOMAIN_NAME);
	if(NULL == ipAddress){
		fprintf(stderr, "ERROR: unable to get ip address\n");
		return EXIT_FAILURE;
	}

	response = tcpConnect(fd, ipAddress, PORT, true);
	free(ipAddress);
	if(NULL != response){
		free(response);
	}

	response = tcpSendData(fd, DATA);
	if(NULL != response){
		write(STDOUT_FILENO, response, strlen(response));
		free(response);
	}

	response = tcpDisconnect(fd);
	if(NULL != response){
		free(response);
	}
	close(fd);
	return EXIT_SUCCESS;
}

void dump(int sig){
  void* array[10];
  size_t size = backtrace(array, 10);
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
	char* response = tcpDisconnect(3);
	write(STDOUT_FILENO, response, strlen(response));
	free(response);
  exit(EXIT_FAILURE);	
}
