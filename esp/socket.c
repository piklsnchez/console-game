#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <error.h>
#include <sys/socket.h>
#include <arpa/inet.h>

int main(int argc, char* argv[]){
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if(0 > sock){
		perror("ERROR");
		return -1;
	}
	struct sockaddr_in serverAddress = {
		.sin_family = AF_INET,
		.sin_port   = htons(80)
	};
	int status = inet_pton(AF_INET, "162.88.193.70", &serverAddress.sin_addr);
	if(0 >= status){
		perror("ERROR");
		return -1;
	}
	status = connect(sock, (struct sockaddr*)&serverAddress, sizeof(serverAddress));
	if(0 > status){
		perror("ERROR");
		return -1;
	}
	char* data = "GET / HTTP1.1\r\n\r\n";
	send(sock, data, strlen(data), 0);
	char buffer[1024];
	int r = read(sock, buffer, sizeof(buffer));
	write(STDOUT_FILENO, buffer, r);
	return 0;
}
