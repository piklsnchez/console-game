#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <poll.h>
#include <termios.h>
#include <string.h>
#include <stdarg.h>
#include "at.h"

#include "private.h"

static const char OK[]                = "AT";	//returns ok
static const char RESTART[]           = "AT+RST";	//restart the module basic	-	-	-	-
static const char ECHO_ON[]           = "ATE1";//echo commands; default
static const char ECHO_OFF[]          = "ATE0";//don't echo commands
static const char WIFI_MODE[]         = "AT+CWMODE";	//wifi mode	wifi AT+CWMODE=<mode>	AT+CWMODE? AT+CWMODE=? 1=Sta, 2=AP, 3=both
static const char JOIN_AP[]           = "AT+CWJAP_DEF";	//join the AP	wifi and save to flash AT+CWJAP_DEF =<ssid>,<pwd> AT+CWJAP? - ssid=ssid, pwd=wifi password
static const char LIST_AP[]           = "AT+CWLAP";	//list the AP	wifi AT+CWLAP			
static const char QUIT_AP[]           = "AT+CWQAP";	//quit the AP	wifi AT+CWQAP - AT+CWQAP=?	
static const char SET_PARAMETERS[]    = "AT+CWSAP";	//set the parameters of AP wifi AT+CWSAP=<ssid>,<pwd>,<chl>,<ecn> AT+CWSAP? ssid,pwd,chl=channel, ecn=encryption Connect to your router: AT+CWJAP="YOURSSID","helloworld"; and check if connected: AT+CWJAP?
static const char CONNECTION_STATUS[] = "AT+CIPSTATUS";	//get the connection status	TCP/IP AT+CIPSTATUS			
static const char SETUP_CONNECTION[]  = "AT+CIPSTART";	//set up TCP or UDP connection TCP/IP 1)single connection (+CIPMUX=0) AT+CIPSTART=<type>,<addr>,<port>; 2) multiple connection (+CIPMUX=1) AT+CIPSTART=<id><type>,<addr>,<port> -	AT+CIPSTART=?	id=0-4, type=TCP/UDP, addr = IP address port=port	Connect to another TCP server, set multiple connection first: AT+CIPMUX=1; connect: AT+CIPSTART=4,"TCP","X1.X2.X3.X4",9999
static const char DNS_LOOKUP[]        = "AT+CIPDOMAIN";//dnslookup AT+CIPDOMAIN=gmail.com; return +CIPDOMAIN:123.456.789.101
static const char SSL_BUFFER_SIZE[]   = "AT+CIPSSLSIZE";
static const char CONFIGURE_SSL[]     = "AT+CIPSSLCCONF";
static const char SEND_DATA[]         = "AT+CIPSEND";	//send data	TCP/IP 1)single connection(+CIPMUX=0) AT+CIPSEND=<length>; 2) multiple connection (+CIPMUX=1) AT+CIPSEND=<id>,<length> AT+CIPSEND=? send data: AT+CIPSEND=4,15 and then enter the data
static const char CLOSE_CONNECTION[]  = "AT+CIPCLOSE";	//close TCP or UDP connection	TCP/IP AT+CIPCLOSE=<id> or AT+CIPCLOSE AT+CIPCLOSE=?	
static const char GET_IP_ADDRESS[]    = "AT+CIFSR";	//Get IP address TCP/IP AT+CIFSR AT+CIFSR=?	
static const char SET_CONNECTION[]    = "AT+CIPMUX";	//set mutiple connection TCP/IP AT+CIPMUX=<mode> AT+CIPMUX?	0 for single connection 1 for mutiple connection
static const char SET_AS_SERVER[]     = "AT+CIPSERVER";	//set as server	TCP/IP AT+CIPSERVER=<mode>[,<port>]	mode 0 to close server mode, mode 1 to open; port=port turn on as a TCP server: AT+CIPSERVER=1,8888, check the self server IP address: AT+CIFSR=?
static const char RECEIVE_DATA[]      = "+IPD";	//received data					

static const char TCP[] = "TCP";
static const char SSL[] = "SSL";

int initTty(){
	PRINT_FUNC;
	int fd = open("/dev/ttyUSB0", O_RDWR);
	if(0 > fd){
		OUTPUT;
		perror("ERROR: ");
		exit(EXIT_FAILURE);//FIXME
	}
	struct termios tty;
	int status = tcgetattr(fd, &tty);
	if(0 != status){
		OUTPUT;
		perror("ERROR: ");
		exit(EXIT_FAILURE);
	}
	//set buad rate
	cfsetispeed(&tty, B115200);
	cfsetospeed(&tty, B115200);
	tty.c_cflag &= ~PARENB; //no parity bits
	tty.c_cflag &= ~CSTOPB; //only 1 stop bit
	tty.c_cflag &= ~CSIZE;
	tty.c_cflag |= CS8; //set to 8 bits
	tty.c_cflag &= ~CRTSCTS; //no 
	tty.c_cflag |= CREAD | CLOCAL;
	tty.c_lflag &= ~ICANON;
	tty.c_lflag &= ~IEXTEN;
	tty.c_lflag &= ~ECHO;
	tty.c_lflag &= ~ECHOE;
	tty.c_lflag &= ~ECHONL;
	tty.c_lflag &= ~ISIG;
	tty.c_iflag &= ~(IXON | IXOFF | IXANY);
	tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL);
	tty.c_oflag &= ~OPOST;
	tty.c_oflag &= ~ONLCR;
	tty.c_cc[VTIME] = 5; //10 per second
	tty.c_cc[VMIN] = 0;//minimum number of characters to read
	status = tcsetattr(fd, TCSANOW, &tty);
	if(0 != status){
		OUTPUT;
		perror("ERROR: ");
		exit(EXIT_FAILURE);//FIXME
	}
 return fd;
}

char* apRestart(int fd){
	PRINT_FUNC;
	return apSendCommand2(fd, "AT+RST\r\n", "\nOK\r\n");
}

bool apEchoOff(int fd){
	PRINT_FUNC;
	const char successResponse[] = "\nOK\r\n";
	char* response = apSendCommand2(fd,"ATE0\r\n", successResponse);
	bool  success  = NULL != strstr(response, successResponse);
	free(response);
	return success;
}

bool apEchoOn(int fd){
	PRINT_FUNC;
	const char successResponse[] = "\nOK\r\n";
	char* response = apSendCommand2(fd, "ATE1\r\n", successResponse);
	bool  success  = NULL != strstr(response, successResponse);
	free(response);
	return success;
}

int getApStatus(int fd){
	PRINT_FUNC;
	const char successResponse[] = "\nOK\r\n";
	char* response = apSendCommand2(fd, "AT+CWJAP_DEF?\r\n", successResponse);
	char* found    = strstr(response, SID);
	free(response);
	return (NULL == found) ? 0 : 1;
}

int getConnectionStatus(int fd){
	PRINT_FUNC;
	const char responseSuccess[] = "\nOK\r\n";
	char  responseString[]       = "STATUS:";
	char  command[]              = "AT+CIPSTATUS\r\n";
	char* response               = apSendCommand2(fd, command, responseSuccess);//STATUS:%d
#ifdef __DEBUG
	OUTPUT;
	write(STDOUT_FILENO, response, strlen(response));
#endif
	int  status;
	bool error = true;
	if(NULL != response){
		char* tmp = strstr(response, responseString);
		if(NULL != tmp){
			tmp += strlen(responseString);
			int count = sscanf(tmp, "%d", &status);
			if(0 < count){
				error = false;
			}
		}
	}
	if(error){
		OUTPUT;
		fprintf(stderr, "ERROR: counldn't get connection status. (%s)|%s|", command, response);
		fflush(stderr);
		free(response);
		return -1;
	}
	free(response);
	return status;
}

bool setApStationMode(int fd){
	PRINT_FUNC;
	const char responseSuccess[] = "\nOK\r\n";
	const char command[]         = "AT+CWMODE=1\r\n";
	char* response               = apSendCommand2(fd, command, responseSuccess);
	bool  success                = endsWith(response, responseSuccess);
	free(response);
	return success;
}

bool apConnect(int fd){
	PRINT_FUNC;
	const char responseSuccess[] = "\nOK\r\n";
	char* command                = newCommand("AT+CWJAP_DEF=\"%s\",\"%s\"\r\n", SID, PASSWORD);
	char* response               = apSendCommand2(fd, command, responseSuccess);
	free(command);
	bool success                 = endsWith(response, responseSuccess);
	free(response);
	return success;
}

/*
 * must free result
 */
char* getIpAddress(int fd){
	PRINT_FUNC;
	bool success = false;
	const char successResponse[] = "\rOK\r\n";
	const char responseString[]  = "+CIFSR:STAIP,";
	const char command[]         = "AT+CIFS\r\n";
	char* response               = apSendCommand2(fd, command, successResponse);
	char* ipAddress = NULL;
	if(NULL != response){
		char* tmp = strstr(response, responseString);
		if(NULL != tmp){
			tmp      += strlen(responseString);
			ipAddress = malloc(16);//192.168.100.100\0
			if(0 < sscanf(tmp, "\"%s\"", ipAddress)){
				ipAddress[strlen(ipAddress) -1] = 0;//trim trailing quote
				success = true;
			}
		}
	}
	if(!success){
		OUTPUT;
		fprintf(stderr, "ERROR: couldn't get ip address. (%s)|%s|", command, response);
		fflush(stderr);
	}
	if(NULL != response){
		free(response);
	}
	return ipAddress;
}

/*
 * must free result
 */
char* dnsLookup(int fd, const char* domain){
	PRINT_FUNC;
	char responseString[] = "+CIPDOMAIN:";
	char* command         = newCommand("%s=\"%s\"\r\n", DNS_LOOKUP, domain);
	char* response        = apSendCommand(fd, command);
	char* tmp             = strstr(response, responseString);
	if(NULL == tmp){
		OUTPUT;
		fprintf(stderr, "ERROR: couldn't lookup dns. (%s)|%s|", command, response);
		fflush(stderr);
		free(command);
		free(response);
		return NULL;
	}
	tmp += strlen(responseString);
	char* ipAddress = malloc(16);
	int count = sscanf(tmp, "%s OK", ipAddress);
	if(0 >= count){
		OUTPUT;
		fprintf(stderr, "ERROR: couldn't lookup dns. (%s)|%s|", command, response);
		fflush(stderr);
		free(command);
		free(response);
		free(ipAddress);
		ipAddress = NULL;
		return NULL;
	}
	free(command);
	free(response);
	return ipAddress;
}

int getSslBufferSize(int fd){
	PRINT_FUNC;
	char responseString[] = "+CIPSSLSIZE:";
	char* command         = newCommand("%s?\r\n", SSL_BUFFER_SIZE);
	char* response        = apSendCommand(fd, command);
	char* tmp             = strstr(response, responseString);
	if(NULL == tmp){
		OUTPUT;
		fprintf(stderr, "ERROR: couldn't get ssl buffer size. (%s)|%s|\n", command, response);
		fflush(stderr);
		exit(EXIT_FAILURE);
	}
	tmp += strlen(responseString);
	int size;
	int count = sscanf(tmp, "%d", &size);
	if(0 == count){
		OUTPUT;
		fprintf(stderr, "ERROR: couldn't get ssl buffer size. (%s)|%s|\n", command, response);
		fflush(stderr);
		exit(EXIT_FAILURE);
	}
	free(command);
	free(response);
	return size;
}

char* setSslBufferSize(int fd, int size){
	PRINT_FUNC;
	char* command  = newCommand("%s=%d\r\n", SSL_BUFFER_SIZE, size);
	char* response = apSendCommand(fd, command);
	free(command);
	return response;
}

/*
 * must free result
 */
char* getSslConfiguration(int fd){
	PRINT_FUNC;
	char* command  = newCommand("%s?\r\n", CONFIGURE_SSL);
	char* response = apSendCommand(fd, command);
	free(command);
	return response;
}

/*
 * must free result
 */
char* configureSsl(int fd){
	PRINT_FUNC;
	char* command  = newCommand("%s=2\r\n", CONFIGURE_SSL);
	char* response = apSendCommand(fd, command);
	free(command);
	return response;
}

/*
 * must free result
 */
char* tcpConnect(int fd, const char* ipAddress, int port, bool ssl){
	PRINT_FUNC;
	char* command  = newCommand("%s=\"%s\",\"%s\",%d\r\n", SETUP_CONNECTION, ssl ? "SSL" : "TCP", ipAddress, port);
	char* response = apSendCommand(fd, command);
	free(command);
	return response;
}

/*
 * must free result
 */
char* tcpDisconnect(int fd){
	PRINT_FUNC;
	char* command  = newCommand("%s\r\n", CLOSE_CONNECTION);
	char* response = apSendCommand(fd, command);
	free(command);
	return response;
}

/*
 * must free result
 * FIXME: need to wait for some excpected response
 */
char* tcpSendData(int fd, const char* data){
	PRINT_FUNC;
	char* command  = newCommand("%s=%d\r\n", SEND_DATA, strlen(data));
	char* response = apSendCommand(fd, command);
	if(NULL == response){
		OUTPUT;
		fprintf(stderr, "ERROR: calling SEND_DATA returned null. (%s)|%s|", command, response);
		fflush(stderr);
		return NULL;
	}
	free(command);
	free(response);
	response = apSendCommand(fd, data);
	if(NULL == response){
		OUTPUT;
		fprintf(stderr, "ERROR: sending data returned null. (%s)|%s|", command, response);
		fflush(stderr);
		return NULL;
	}
	char* parsedResponse = parseResponse(response);
	free(response);
	return parsedResponse;
}

char* apSendCommand2(int fd, const char* command, const char* success){
	PRINT_FUNC;
	write(fd, command, strlen(command));
	char* response = NULL;
	char  c;
	int   length = 0;
	for(int i = 30; i > 0;){
		if(0 == read(fd, &c, 1)){
			i--;
			continue;
		}
		response            = realloc(response, ++length + 1);
		response[length -1] = c;
		response[length]    = 0;
		if(0 == strcmp(response + length - strlen(success), success)){
#ifdef __DEBUG
			OUTPUT;
			printf("found\n");
			fflush(stdout);
#endif
			break;
		}
	}
	return response;
}

/*
 * must free result
 */
char* apSendCommand(int fd, const char* command){//FIXME: returns busy p... etc; close connection
	char* results[7] = {"\nOK\r\n\0", "\nSEND OK\r\n\0", "\nERROR\r\n\0", "\nFAIL\r\n\0", "\nCLOSED\r\n\0", "\nbusy p...\r\n\0", NULL};
	write(fd, command, strlen(command));
	char* response = NULL;
	char  c;
	int   length = 0;
	for(int i = 0; i < 30 && !anyMatch(response, results); i++){
		while(0 < read(fd, &c, 1)){
			response            = realloc(response, ++length);
			response[length -1] = c;
		}
	}
	response            = realloc(response, ++length);
	response[length -1] = 0;
	if(NULL == strstr(response, results[0]) && NULL == strstr(response, results[1])){
		if(NULL != strstr(response, "\nbusy p...\r\n")){
			OUTPUT;
			fprintf(stderr, "ERROR: %s\nrestarting\n", response);
			fflush(stderr);
			free(response);
			response = apRestart(fd);
			write(STDOUT_FILENO, response, strlen(response));
			free(response);
			exit(EXIT_FAILURE);
		}
		OUTPUT;
		fprintf(stderr, "Didn't find \"OK\": (%s)|%s|", command, response);
		fflush(stderr);
		free(response);
		response = NULL;
	}
	return response;
}

/*
 * must free result
 */
char* newCommand(const char* format, ...){
	va_list args;
	va_start(args, format);
	int len = vsnprintf(NULL, 0, format, args) +1;
	char* command = malloc(len);
	vsnprintf(command, len, format, args);
	return command;
}

bool anyMatch(char* haystack, char* needles[]){
	if(NULL == haystack || 0 == strlen(haystack)){
		return false;
	}
	for(int i = 0; NULL != needles[i]; i++){
		if(NULL != strstr(haystack, needles[i])){
			return true;
		}
	}
	return false;
}

/*
 * must free result
 */
char* parseResponse(char* response){
	PRINT_FUNC;
	char* returnString = NULL;
	char* substring    = response;
	while(NULL != (substring = strstr(substring, "+IPD,"))){
		int responseLength;
		if(0 >= sscanf(substring, "+IPD,%d", &responseLength)){
			OUTPUT;
			fprintf(stderr, "ERROR: |%s|\n", substring);
			fflush(stderr);
			free(returnString);
			return NULL;
		}
		char* tmp = strchr(substring, ':');
		if(NULL == tmp){
			OUTPUT;
			fprintf(stderr, "ERROR: response doesn't contain :\n");
			fflush(stderr);
			free(returnString);
			return NULL;
		}
		substring = tmp +1;
		int len = 0;
		if(NULL != returnString){
			len = strlen(returnString);
		}
		returnString = realloc(returnString, len + responseLength + 1);
		if(NULL == returnString){
			OUTPUT;
			fprintf(stderr, "Couldn't realloc returnString\n");
			fflush(stderr);
			return NULL;
		}
		strncat(returnString, substring, responseLength);
	}
#ifdef __DEBUG
	if(NULL == returnString){
		OUTPUT;
		printf("INFO: response: %s", response);
		fflush(stdout);
	}
#endif
	return returnString;
}

bool endsWith(const char* string, const char* end){
	return 0 == strcmp(string + strlen(string) -strlen(end), end);
}
