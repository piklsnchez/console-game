cd /usr/local/git
# download esptool
git clone https://github.com/espressif/esptool
# download the Espressif SDK 2.2.0 https://github.com/espressif/ESP8266_NONOS_SDK/archive/v2.2.0.zip or clone it
git clone https://github.com/espressif/ESP8266_NONOS_SDK -branch release/v2.2.x

# Note that on wemos D1 mini, the data must be flashed in 0x3fc000 as we have 4 MB (32Mbit), this apply to modules like ESP-12E, NodeMCU devkit 1.0 as well
cd -
exit

python esptool/esptool.py -port=/dev/ttyUSB0 \
-chip esp8266 write_flash -fm dio -ff 20m -fs detect \
0x0000 ESP8266_NONOS_SDK/bin/boot_v1.7.bin \
0x01000 ESP8266_NONOS_SDK/bin/at/1024+1024/user1.2048.new.5.bin \
0x3fc000 ESP8266_NONOS_SDK/bin/esp_init_data_default_v05.bin \
0x7e000 ESP8266_NONOS_SDK/bin/blank.bin \
0x3fe000 ESP8266_NONOS_SDK/bin/blank.bin
