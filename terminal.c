#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>//struct winsize
#include <sys/ioctl.h>//struct winsize
#include "point.h"
#include "command.h"
#include "logger.h"
#include "terminal.h"

struct winsize terminal_screenSize;
struct termios orig_termios;
#define BUFFER_SIZE 5
char buffer[BUFFER_SIZE];
int buffered;
static logger_t* logger;

void terminal_init(){
  logger = logger_new("terminal.log");
}

void enableCookedMode() {
  if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig_termios) == -1){
    perror("tcsetattr");
	}
}

void enableRawMode() {
  if (tcgetattr(STDIN_FILENO, &orig_termios) == -1){
    perror("tcgetattr");
  }
  atexit(enableCookedMode);
  struct termios raw = orig_termios;
  raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
  raw.c_oflag &= ~(OPOST);
  raw.c_cflag |= (CS8);
  raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
  raw.c_cc[VMIN] = 0;
  raw.c_cc[VTIME] = 0;
  if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) == -1){
    perror("tcsetattr");
  }
}

struct winsize* getScreenSize(){
  ioctl(STDIN_FILENO, TIOCGWINSZ, &terminal_screenSize);
  return &terminal_screenSize;
}

void waitForInput(){
	int size;
	while(0 == (size = read(STDIN_FILENO, NULL, 1))){}
}

int getBuffered(){
	return buffered;
}

char* readChar(){
	do {
		readCharNoWait();
	} while(NULL == buffer);
	return buffer;
}

char* readCharNoWait(){
  memset(buffer, 0, BUFFER_SIZE);
  bool done = true;
  char c;
  int r;
  do {
    r = read(STDIN_FILENO, &c, 1);
    if(0 != r){
      if(ESC[0] == c || '[' == c){
        done = false;
      } else if(!done && (c >= 0x30 && c <= 0x7e)){//end of escape sequence
        done = true;
      }
      buffer[strlen(buffer)] = c;
    }
  } while(!done);
  ioctl(STDIN_FILENO, FIONREAD, &buffered);
  if(0 < buffered){
    char tmp[buffered];
    tmp[buffered];
    read(STDIN_FILENO, tmp, buffered);
    char message[55];
    snprintf(message, sizeof(message), "throwing awway %d chars |%s|", buffered, tmp);
    logger->doInfo(logger, message);
  }
  return buffer;
}

void writeChars(char* c){
  if(0 == c[0]){
    return;
  }
  write(STDOUT_FILENO, c, strlen(c));
}

void fillScreen(struct winsize screenSize, const char c){
	storeCursorPosition();
	for(int row = 0; row < screenSize.ws_row; row++){
		for(int col = 0; col < screenSize.ws_col; col++){
			struct point p = {col, row};
			setCursorPosition(&p);
			write(STDOUT_FILENO, &c, 1);
		}
	}
	restoreCursorPosition();
}
