#include <unistd.h>
#include <error.h>
#include <time.h>
#include <stdint.h>
#include <inttypes.h>
#include "logger.h"

static void logger_free(logger_t* this){
  fclose(this->_logFile);
  free(this);
}

static void logger_doInfo(logger_t* this, const char* message){
  struct timespec now;
  clock_gettime(CLOCK_MONOTONIC_RAW, &now);
  uint64_t ms = (now.tv_sec * 1000000) + (now.tv_nsec / 1000);//milliseconds / 1000
  fprintf(this->_logFile, "%" PRIu64 " INFO: %s\n", ms,  message);
}

logger_t* logger_new(const char* logFile){
  logger_t* this = malloc(sizeof(logger_t));
  this->_logFile = fopen(logFile, "a");
  if(NULL == this->_logFile){
    perror("ERROR");
    return NULL;
  }
  this->free     = logger_free;
  this->doInfo   = logger_doInfo;
  return this;
}
