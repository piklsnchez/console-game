#include <stdlib.h>
#include <fcntl.h>//open
#include <stdio.h>
#include <poll.h>//poll fd
#include <stdbool.h>
#include <unistd.h>
#include <execinfo.h>//stacktrace
#include <signal.h>//stacktrace
#include <strings.h>
#include <string.h>
#include <time.h>
#include <sys/ioctl.h>//struct winsize
#include <termios.h>//struct winsize
#include <time.h>//clock_nanosleep
#include "terminal.h"
#include "point.h"
#include "command.h"
#include "font.h"
#include "readInput.h"//input_event
#include "logger.h"

#define BILLION 1000000000
#define HUNDERED_MILLION (BILLION / 10)

void init(struct winsize* screenSize);
void loop(struct winsize* screenSize);
void readInput();
void movePlayerLeft(struct winsize* screenSize);
void movePlayerRight(struct winsize* screenSize);
void shootMissile();
void moveMissiles(struct winsize* screenSize);
bool checkCollision(struct winsize* screenSize);
void moveEnemies(struct winsize* screenSize);
void displayEnemies();
void displayStatus(struct winsize* screenSize);
void syncFrame();
void doWin(struct winsize* screenSize);
void doLose(struct winsize* screenSize);
void drawChar(char c, struct winsize* screenSize, struct point* position);
void drawString(char* s, struct winsize* screenSize, struct point* position);

static logger_t logger;
int    readInputFd;
enum   Direction {NONE, LEFT, RIGHT} direction;
bool   fire = false;
struct timespec until = {0,0};
struct timespec dtime;
int    offset = 1;
int    enemyDirection = 1;
struct point playerPosition;
#define MISSILES 10
struct point* missiles[MISSILES];
#define ENEMY_SIZE 12
struct point* enemies[ENEMY_SIZE];
#define ENEMY_MISSILES_SIZE 1
struct point* enemyMissiles[ENEMY_MISSILES_SIZE];
int    debugIoWaitingSize;
bool   useEvent = true;

struct reader {
  enum Direction direction;
  enum Direction (*getDirection)(struct reader* this);
  bool (*getFired)(struct reader* this);
} typedef reader_t;

enum Direction reader_getDirection(reader_t* this){
  char* chars = readCharNoWait();
  if(3 == strlen(chars)){//67 right; 68 left
    int d = chars[2];
    switch(d){
      case 67: return RIGHT;
      case 68: return LEFT;
    }
  }
  return NONE;
}

bool reader_getFired(reader_t* this){
  char* chars = readCharNoWait();
  if(1 == strlen(chars)){
    if(' ' == chars[0]){
      return true;
    }
  }
  return false;
}

reader_t* reader_new(){
  reader_t* reader = malloc(sizeof(reader_t));
  reader->getDirection = reader_getDirection;
  reader->getFired     = reader_getFired;
  return reader;
}
reader_t* reader = NULL;

void dump(int sig){
  enableCookedMode();
  void *array[10];
  size_t size = backtrace(array, 10);
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}

int main(int argc, char* argv[]){
  signal(SIGSEGV, dump);
  logger = *logger_new("game.log");
  struct winsize* screenSize = getScreenSize();
  init(screenSize);
  loop(screenSize);
  free(screenSize);
  return 0;
}

/*sets readInputFd, direction, missles, enemies, enemyMissle, playerPosition; initialize random number; enableRawMode*/
void init(struct winsize* screenSize){
  readInputFd = open("/dev/input/event0", O_RDONLY);
  if(-1 == readInputFd){
    fprintf(stderr, "ERROR: couldn't open input\n");
    useEvent = false;
  }
  direction = NONE;
  srand((unsigned)time(NULL));
  terminal_init();
  enableRawMode();
  if(!useEvent){
    reader = reader_new();
  }

  for(int i = 0; i < ENEMY_SIZE; i++){
    enemies[i] = malloc(sizeof(struct point));
  }

  for(int i = 0; i < MISSILES; i++){
    missiles[i] = NULL;
  }

  for(int i = 0; i < ENEMY_MISSILES_SIZE; i++){
    enemyMissiles[i] = NULL;
  }
  playerPosition.x = screenSize->ws_col / 2;
  playerPosition.y = screenSize->ws_row - 1;
  setCursorPosition(&playerPosition);
}

void loop(struct winsize* screenSize){
  while(true){
    if(useEvent){
      readInput();
    } else {
      logger.doInfo(&logger, "before getDirection");
      direction = reader->getDirection(reader);
      logger.doInfo(&logger, "after getDirection");
    }
    logger.doInfo(&logger, "before direction");
    switch(direction){
      case RIGHT: movePlayerRight(screenSize);
      break;
      case LEFT: movePlayerLeft(screenSize);
      break;
      case NONE:
      break;
    }
    logger.doInfo(&logger, "after direction");
    if(!useEvent){
      logger.doInfo(&logger, "before getFired");
      fire = reader->getFired(reader);
      logger.doInfo(&logger, "after getFired");
    }
    if(fire){
      logger.doInfo(&logger, "before shootMissle");
      shootMissile();
      logger.doInfo(&logger, "after shootMissile");
    }
    logger.doInfo(&logger, "before moveEnemies");
    moveEnemies(screenSize);
    logger.doInfo(&logger, "after moveEnemies");
    logger.doInfo(&logger, "before clearScreen");
    clearScreen();
    logger.doInfo(&logger, "after clearScreen");
    logger.doInfo(&logger, "before displayEnemies");
    displayEnemies();
    logger.doInfo(&logger, "after displayEnemies");
    logger.doInfo(&logger, "before moveMissiles");
    moveMissiles(screenSize);
    logger.doInfo(&logger, "after moveMissiles");
    logger.doInfo(&logger, "before checkCollision");
    checkCollision(screenSize);
    logger.doInfo(&logger, "after checkCollision");
    logger.doInfo(&logger, "before displayStatus");
    displayStatus(screenSize);
    logger.doInfo(&logger, "after displayStatus");
    logger.doInfo(&logger, "before setCursorPosition");
    setCursorPosition(&playerPosition);
    logger.doInfo(&logger, "after setCursorPosition");
    syncFrame();
  }
}

void readInput(){
  int available;
  struct pollfd poller;
  poller.fd     = readInputFd;
  poller.events = POLLIN;
  while(0 < (available = poll(&poller, 1, 0))){
    struct input_event event;
    int r = read(readInputFd, &event, sizeof(struct input_event));
/*debug*/debugIoWaitingSize = r;
    if(1 == event.type){
      switch(event.code){
        case 105: //left
          if(1 == event.value){//keydown
            direction = LEFT;
          } else if(0 == event.value){
            direction = NONE;
          }
        break;
        case 106://right
          if(1 == event.value){//keydown
            direction = RIGHT;
          } else if(0 == event.value){
            direction = NONE;
          }
        break;
        case 57://space
          if(1 == event.value){
            fire = true;
          } else if(0 == event.value){
            fire = false;
          }
        break;
        case 16://q
          if(1 == event.value){
            exit(0);
          }
        break;
      }
    }
  }
}

void movePlayerRight(struct winsize* screenSize){
  if(playerPosition.x < screenSize->ws_col){
    playerPosition.x++;
    setCursorPosition(&playerPosition);
  }
}

void movePlayerLeft(struct winsize* screenSize){
  if(playerPosition.x > 1){
    playerPosition.x--;
    setCursorPosition(&playerPosition);
  }
}

void shootMissile(){
  write(STDOUT_FILENO, "+", 1);
  for(int i = 0; i < MISSILES; i++){
    if(NULL == missiles[i]){
      missiles[i]    = malloc(sizeof(struct point));
      missiles[i]->x = playerPosition.x;
      missiles[i]->y = playerPosition.y;
      break;
    }
  }
}

void moveMissiles(struct winsize* screenSize){
  //player missile
  for(int i = 0; i < MISSILES; i++){
    if(NULL == missiles[i]){
      continue;
    }
    if(missiles[i]->y > 0){
      missiles[i]->y--;
      setCursorPosition(missiles[i]);
      write(STDOUT_FILENO,"+", 1);
    } else {
      free(missiles[i]);
      missiles[i] = NULL;
    }
  }
  //enemy missile
  for(int i = 0; i < ENEMY_MISSILES_SIZE; i++){
    if(NULL == enemyMissiles[i]){
      continue;
    }
    if(enemyMissiles[i]->y <= screenSize->ws_row){
      enemyMissiles[i]->y++;
      setCursorPosition(enemyMissiles[i]);
      write(STDOUT_FILENO, "!", 1);
    } else {
      free(enemyMissiles[i]);
      enemyMissiles[i] = NULL;
    }
  }
}

bool checkCollision(struct winsize* screenSize){
	bool result = false;
	//player missile
	for(int i = 0; i < ENEMY_SIZE; i++){
		for(int j = 0; j < MISSILES; j++){
			if(NULL == enemies[i]){
				continue;
			}
			struct point p1;
			p1.x = enemies[i]->x + 1;
			p1.y = enemies[i]->y;
			struct point p2;
			p2.x = enemies[i]->x;
			p2.y = enemies[i]->y + 1;
			struct point p3;
			p3.x = enemies[i]->x + 1;
			p3.y = enemies[i]->y + 1;
			if(  pointEquals(missiles[j], enemies[i])
				|| pointEquals(missiles[j], &p1)
				|| pointEquals(missiles[j], &p2)
				|| pointEquals(missiles[j], &p3)
			){
				free(enemies[i]);
				enemies[i] = NULL;
				if(0 == pointNumberOf(enemies, ENEMY_SIZE)){
					doWin(screenSize);
				}
				free(missiles[j]);
				missiles[j] = NULL;
				result      = true;
			}
		}
	}
	//enemy missile
	for(int i = 0; i < ENEMY_MISSILES_SIZE; i++){
		if(NULL == enemyMissiles[i]){
			continue;
		}
		if(/*point*/pointEquals(enemyMissiles[i], &playerPosition)){
			free(enemyMissiles[i]);
			enemyMissiles[i] = NULL;
			doLose(screenSize);
		}
	}
	return result;
}

void moveEnemies(struct winsize* screenSize){
	if(offset > 20 || offset <= 0){//offset is the distance to the right of the enemies original spot
		enemyDirection *= -1;// -1 for left; 1 for right
	}
	offset += enemyDirection;//move them
	int verticalGap   = 5;//how far apart vertically
	int horizontalGap = 50;//how far appart horizontally
	int width         = 1 + screenSize->ws_col;//how wide is the screen
	int r             = rand() % ENEMY_SIZE;//pick a random enemy
	for(int i = 0; i < ENEMY_SIZE; i++){
		int slot = i * horizontalGap;//enemy i's horizontal screen position
		if(NULL != enemies[i]){//this enemy might be dead
			enemies[i]->x = 1 + ((offset + slot) % width);//horizontal position (mod because there are multiple rows)
			enemies[i]->y = 1 + (( (offset + slot) / width) * verticalGap);//vertical position
			if(i == r){//r is the enemy that will shoot a missile
				for(int j = 0; j < ENEMY_MISSILES_SIZE; j++){
					if(NULL == enemyMissiles[j]){//limit number of enemy missiles
						enemyMissiles[j] = malloc(sizeof(struct point));//create the missile
						enemyMissiles[j]->x = enemies[i]->x;
						enemyMissiles[j]->y = enemies[i]->y;
					}
				}
			}
		}
	}
}

void displayEnemies(){
	for(int i = 0; i < ENEMY_SIZE; i++){
		if(NULL != enemies[i]){//if the enemy is not already dead
			setCursorPosition(enemies[i]);
			char c[] = {'#', '#'};//enemies are 2x2 chars
			write(STDOUT_FILENO, c, 2);//top row
			struct point p;
			p.x = enemies[i]->x;
			p.y = enemies[i]->y + 1;
			setCursorPosition(&p);
			write(STDOUT_FILENO, c, 2);//bottom row
		}
	}
}

/*This is text to display at the bottom of the screen*/
void displayStatus(struct winsize* screenSize){
  //left side
  int    numberOfEnemies = pointNumberOf(enemies, ENEMY_SIZE);//how many enemies are left?
  char   output[45];
  int    length = snprintf(output, sizeof(output), "Enemies: %d ", numberOfEnemies);//set the data of the string
  struct point p        = {0, screenSize->ws_row};//where on screen will we put the string?
  setCursorPosition(&p);
  write(STDOUT_FILENO, output, length);//write to screen
  
  //right side
  char* formatString =  "until: %d, %d position: x:%d,y:%d";
  struct timespec currentTime;
  clock_gettime(CLOCK_MONOTONIC, &currentTime);
  length = snprintf(output, sizeof(output), formatString, until.tv_sec - currentTime.tv_sec, until.tv_nsec - currentTime.tv_nsec, playerPosition.x, playerPosition.y);//set the data of the string
  p.x = screenSize->ws_col - length - 5;//right justification
  setCursorPosition(&p);
  write(STDOUT_FILENO, output, length);//write to the screen
}

/*this is to attempt to get a constant frame rate*/
void syncFrame(){
  clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &until, NULL);
  struct timespec old = {until.tv_sec, until.tv_nsec};
  clock_gettime(CLOCK_MONOTONIC, &until);
  dtime.tv_sec  = until.tv_sec  - old.tv_sec;
  dtime.tv_nsec = until.tv_nsec - old.tv_nsec;
  if(0 > dtime.tv_nsec){
    dtime.tv_sec -= 1;
    dtime.tv_nsec += BILLION;//1000000000;
  }
  until.tv_nsec += HUNDERED_MILLION;//100000000;
  if(until.tv_nsec >= BILLION){
    until.tv_sec++;
    until.tv_nsec -= BILLION;
  }
}

void doLose(struct winsize* screenSize){
	struct point p = {10, 10};
	char* lose = "lose";
	drawString(lose, screenSize, &p);
	exit(0);
}

void doWin(struct winsize* screenSize){
	struct point p = {10, 10};
	char* win = "win";
	drawString(win, screenSize, &p);
	exit(0);
}

void drawChar(char c, struct winsize* screenSize, struct point* position){
	struct point p;
	p.x = position->x;
	p.y = position->y;
	setCursorPosition(&p);
	char solid = '*';
	char empty = ' ';
	for(int y = 0; y < FONT_HEIGHT; y++){
		for(int x = 0; x < FONT_WIDTH; x++){
			int yy = y + ((c - 'a') * FONT_HEIGHT);//'a' is at offset 0
			bool isSolid = font[yy][x];
			char _c = isSolid ? solid : empty;
			write(STDOUT_FILENO, &_c, 1);//display an an asterisk or a space
			p.x++;
			setCursorPosition(&p);
		}
		p.x = position->x;
		p.y++;
		setCursorPosition(&p);
	}
}

void drawString(char* s, struct winsize* screenSize, struct point* position){
	struct point p;
	p.x = position->x;
	p.y = position->y;
	for(int i = 0; i < strlen(s); i++){
		char c = s[i];
		drawChar(c, screenSize, &p);
		p.x += FONT_WIDTH;
	}
}
