#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "point.h"
#include "command.h"

void clearScreen(){
	char* command = ESC"[2J";
	write(STDOUT_FILENO, command, strlen(command));
}

void storeCursorPosition(){
	char* command = ESC"7";
	write(STDOUT_FILENO, command, strlen(command));
}

void restoreCursorPosition(){
	char* command = ESC"8";
	write(STDOUT_FILENO, command, strlen(command));
}

void setCursorPosition(struct point* position){
	int size      = snprintf(NULL, 0, ESC"[%d;%dH", position->x, position->y);
	char* command = malloc(size);
	snprintf(command, size + 1, ESC"[%d;%dH", position->y, position->x);
	write(STDOUT_FILENO, command, size);
	free(command);
}

int getCursorPosition(struct point* position){
	int r = 0;
	char* command = ESC"[6n";
	write(STDOUT_FILENO, command, strlen(command));
	char b[11] = {0};
	while(r < 6){
		r = read(STDIN_FILENO, b, 10);
	}
	return sscanf(b, ESC"[%d;%dR", &position->y, &position->x);
}

void scroll(){
	char command[] = ESC"D";
	write(STDOUT_FILENO, command, sizeof(command));
}

bool isEscape(char* buffer){
	return ESC[0] == buffer[0];
}
