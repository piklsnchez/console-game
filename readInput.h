#ifndef _READ_INPUT_H
#define _READ_INPUT_H
struct input_event {
	struct timeval time;
	unsigned short type;
	unsigned short code;
	unsigned int value;
};


#endif
