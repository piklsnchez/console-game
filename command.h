#ifndef _COMMAND_H
#define _COMMAND_H
#include <stdbool.h>
#include "point.h"

#define ESC "\x1b"
void clearScreen();
void storeCursorPosition();
void restoreCursorPosition();
void setCursorPosition(struct point* position);
int  getCursorPosition(struct point* position);//:(
void scroll();
bool isEscape(char* buffer);
#endif
